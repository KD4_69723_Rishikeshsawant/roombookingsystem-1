import { useState } from 'react';

function VerifyOTPScreen() {
  const [otp, setOTP] = useState('');

  const handleOTPChange = (event) => {
    setOTP(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Handle OTP verification logic here
  };

  return (
    <div>
      <h1>Verify OTP</h1>
      <form onSubmit={handleSubmit}>
        <label>
          OTP:
          <input type="text" value={otp} onChange={handleOTPChange} required />
        </label>
        <button type="submit">Verify</button>
      </form>
    </div>
  );
}

export default VerifyOTPScreen;
