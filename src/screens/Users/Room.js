import React from "react";
import "./Room.css";

const Card = ({ title, imageSrc, description, price, handleRoomSelect }) => {
  const handleBookNow = () => {
    handleRoomSelect(title);
  };

  return (
    <div className="card">
      <img src={imageSrc} alt={title} width="300" height="200"/>
      <h3>{title}</h3>
      <p>{description}</p>
      <h1>{price}</h1>
      {/* <button onClick={handleBookNow}>Book Now</button> */}
    </div>
  );
};

const Room = ({ handleRoomSelect }) => {
  return (
    <div className="App card-container">
      <Card
        title="Delux Room"
        imageSrc="https://s7d2.scene7.com/is/image/ritzcarlton/pnqrz-king-50668318?%E2%82%B9XlargeViewport100pct%E2%82%B9%22"
        description=" Our Deluxe Rooms are perfect for couples, families, and business travelers looking for a luxurious and relaxing stay. Enjoy a range of amenities, including a flat-screen TV, high-speed internet, a mini-fridge, and an en-suite bathroom with premium toiletries. "
        price="₹1200" 
        handleRoomSelect={handleRoomSelect}
      />
      <Card
        title="Non Delux Room"
        imageSrc="https://5.imimg.com/data5/MT/BR/GLADMIN-9295771/deluxe-single-non-ac-room-500x500.png"
        description="Our Non-Deluxe Rooms provide a convenient and affordable option for guests who want to experience everything our hotel has to offer without breaking the bank."
        price="₹800" 
        handleRoomSelect={handleRoomSelect}
     />
    </div>

    
    
  );
};

export default Room;
